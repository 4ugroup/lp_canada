$('.review-more').click(function(e){
    e.preventDefault();
    $('.review-hide').show();
    $('.review-more').hide();
});


//  Modal

$(".btn-modal").fancybox({
    'padding'    : 0,
    'maxWidth'   : 1000,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"><i class="fa fa-close"></i></a>'
    }
});


$('.timer').countdown_sg(15);